const router = require('express').Router();
const productController =require('./product.controller');


// /middlewares
// file upload middleware
const authenticate = require('../../middlewares/authenticate');
const uploader = require('./../../middlewares/uploader');


router.route('/')
    .get(authenticate, productController.find)
    .post(authenticate, uploader.array('image'), productController.insert)

router.route('/add-review/:productId')
    .post(authenticate,productController.addReview)

router.route('/search')
    .get(productController.search)
    .post(productController.search)

router.route('/:id')
    .get(authenticate, productController.findById)
    // .post(productController)
    .put(authenticate, uploader.array('image'), productController.update)
    .delete(authenticate, productController.remove);

module.exports = router;