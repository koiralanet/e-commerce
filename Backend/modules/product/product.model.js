const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const reviewSchema = new Schema({
    point: {
        type: Number,
        min: 1,
        max: 5
    },
    message: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
},{
    timestamps: true
})

const productSchema = new Schema ({

    name: String,
    description: String,
    category: {
        type: String,
        required: true
    },
    color: [String],
    brand: String,
    price: Number,
    quantity: Number,
    images: [String], //array of string
    reviews: [reviewSchema], //array of objects
    status: {
        type: String,
        enum: ['out of stock', 'ordered', 'available'],
        default: 'available'
    },
    tags: [String],
    vendor:{
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            emun: ['quantity', 'percentage', 'value']
        },
        discountValue: String
    },
    warrentyStatus: Boolean,
    warrentyPeriod: String,
    isReturnEligible: Boolean,
    salesDate: Date,
    purchasedDate: Date,
    manuDate: Date,
    expiryDate: Date

}, {
    timestamps: true
})

module.exports = mongoose.model('product', productSchema);