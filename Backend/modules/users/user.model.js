const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
//database modeling
    name: {
        type: String
    },
    email:{
        type: String,
        unique: true
    },
    username:{
        type: String,
        lowercase: true,
        trim: true,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 8
    },
    dob: {
        type: Date,
    },
    phoneNumber: {
        type: Number,  
    },
    address:
     {
        tempAddress:[String], 
        permanentAddress: String
        //type: String
    },
    country: {
        type: String,
        default: 'Nepal'
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others'] // Yo tinta value bhaek database ma kai ni jana paudaina(by using enum)
    },
    status: {
        type: String,
        emun: ['active', 'inActive', 'Idle'],
        default: 'active'
    },
    role: {
        type: String, //1 for admin,2 for normal user
        enum: ['buyer', 'seller', 'admin']
        // default: 2
    },
    image: String
}, {
    timestamps: true
});
//Timestamps will be automated

module.exports = mongoose.model('user', userSchema);