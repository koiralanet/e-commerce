const mongodb =  require('mongodb');
const MongoClient = mongodb.MongoClient;
const dbName = 'group34';
const connectionURL = 'mongodb://localhost:27017';
const OID = mongodb.ObjectID;

module.exports ={
    MongoClient,
    dbName,
    connectionURL,
    OID
}