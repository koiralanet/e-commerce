import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.components';

const container = document.getElementById('root');

ReactDOM.render(<App />, container);

// compnonent
// component is basic building block for react
// component always returns single HTML node
// component can be ==> stateless component, statefull component 

// component can be writtene as ==> Class based component, functional component

// >17 (react version)
// before 16.8 class based component for statefull, functional component for stateless
// Now (>16.8) function compnonet can be statefull as well as stateless
// hooks


// State
// Props

// state and props both are application (componenet) data

// Props ==> incomming data for an component 
// State ==> data within a component ==> Class based component 

// using react we create our own element and define our own attribute