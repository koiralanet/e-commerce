import React from 'react';
import './Login.components.css';
import { Link } from 'react-router-dom';
import { notify } from './../../../utils/toastr';
import { handleError } from '../../../utils/errorHandler';
import { Redirect } from '../../../utils/util';
import { httpClient } from '../../../utils/httpClient';
import { SubmitButton } from '../../Common/SubmitButton/SubmitButton.components';


const defaultForm = {
    username: '',
    password: ''
}

export class Login extends React.Component {
    constructor(){
        super();
        this.state = {
            data:{
                ...defaultForm
            },
            error:{
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm : false,
            remember_me: false
        }
    }

    //TODO life cycle hook and usage of props
    componentDidMount(){
        var token = localStorage.getItem('token');
        if(token){
            this.props.history.push('dashboard')
        }
    }


    onSubmit = e => {
        e.preventDefault();
        
                let isValidForm = this.validateForm();
                if(!isValidForm) return;

                this.setState({
                    isSubmitting: true
                })
        
      
        //API 
        httpClient
        .POST(`/auth/login`, this.state.data)
        .then((response) => {
            notify.showSucuess(`welcome ${response.data.user.username}`);
            // localstorage setup
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('user', JSON.stringify(response.data.user));
            localStorage.setItem('remember_me', this.state.remember_me);

            // navigate to Dashboard
            Redirect.redirectToDashboard(this.props.history, response);
        })
        .catch(err =>{
            handleError(err);
            
        this.setState({
            isSubmitting: false
        })
        })
        
        
    }

    handleChange = e => {

        let {name,value, type, checked } = e.target;
        if(type === 'checkbox'){
            return this.setState({
                remember_me: checked
            })
        } 
        
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }),
        ()=>{
            if(this.state.error[name]){
                this.validateForm()
            }
        }
        )
        
            

    }

    validateForm = () => {
            let usernameErr = this.state.data['username'] ? ''  :'required field*'
            let passwordErr = this.state.data['password'] ? ''  :'required field*'
            console.log("username Err >> ", usernameErr)
        
        this.setState({
            error:{
                username : usernameErr,
                password: passwordErr
            }
        })
        let validForm =  !(usernameErr ||  passwordErr)
        console.log('validForm is >>', validForm)
        return validForm;
    }



    render(){

        return(
            <div className="loginForm">
                <h2> Login </h2>
                <p> Please Login To Start Your Session</p>
                <div className='form-wrapper'>
            <form onSubmit={this.onSubmit}>

                <label htmlFor="username" className="form-label" > Username </label>
                <input type="text" name="username"  className="form-control" id="username" onChange={this.handleChange} placeholder="Username"></input>
                <p className="error">{this.state.error.username}</p>
                <label htmlFor="password" className="form-label"> Password </label>
                <input type="password" name="password" className="form-control" id="password" onChange={this.handleChange} placeholder="Password"></input>
                <p className="error">{this.state.error.password}</p>
                <input type="checkbox" name="remember_me" onChange={this.handleChange} id="check_box"></input>
                <label htmlFor="check_box"> &nbsp; Remember me  </label>
            <br />
            <br />
               <SubmitButton
               isSubmitting={this.state.isSubmitting}
               enabledLabel = 'Login'
               disabledLabel = 'Logining...'
               ></SubmitButton>

            </form>
            <br />
            <p> Don't have an Account?</p>
            <p style={{float:"left"}}> Register <Link to="/register/"> here </Link></p>
            <p style={{float:"right"}}><Link to="/forgot-password">Forgot Password?</Link></p>
            </div>
            </div>
        )
    }
}