//Functional component
//After react 17V import of react os optional

import React from 'react';
import './Header.components.css';
import { NavLink , withRouter } from 'react-router-dom';

const logout = history =>{
    //clear localStorage
    localStorage.clear();
    history.push('/');
    // Navigate to login Page
}

const HeaderComponent = (props) => {
    // const currentUser = JSON.parse(localStorage.getItem('user'));
    let content = props.isLoggedIn
        ? <ul className="nav_list">

            <li className="nav_item">
            <img src="./images/pie.svg" className="dashboard-svg svg" alt="dashbaord.jpg"></img> &nbsp;
            <NavLink to='/dashboard' activeClassName="selected"> Dashboard </NavLink>
            </li>

            <li className="nav_item">
                <img src="./images/projects.svg" className="product-svg svg" alt="project.jpg"></img> &nbsp;
            <NavLink activeClassName="selected" to ='/view_products'>Product</NavLink>
            </li>

            <li className="nav_item">
            <i className="fas fa-comment-dots icon" alt="message.jpg"></i>  &nbsp;
            <NavLink activeClassName="selected" to ='/messages'>Messages</NavLink>
            </li>

            <li className="nav_item">
                <img src="./images/info-button.svg" className="about-svg svg" alt="infoBtn.jpg"></img> &nbsp;
            <NavLink to='/about' activeClassName="selected"> About </NavLink>
            </li>

            <li className="nav_item">
            <img src="./images/settings.svg" className="settings-svg svg" alt="settings.jpg"></img> &nbsp;
            <NavLink to='/settings' activeClassName="selected"> Settings </NavLink>
            </li>
                {/* <p className = " userInfo">  {currentUser.username} </p> */}
            <li className="nav_item">
            <img src="./images/logout.svg" className="logout-svg svg" alt="logout.jpg"></img> &nbsp;
            <button className='logout' onClick={()=>logout(props.history)}> Log out  </button>
            </li>
            </ul>

        : <ul className="nav_list">
            <li className="nav_item" >
            <NavLink to='/home' activeClassName="selected"> Home </NavLink>
            </li>

            <li className="nav_item"> 
            <NavLink to='/' exact activeClassName="selected"> Login </NavLink>
            </li>
            <li className="nav_item">
            <NavLink to='/register' exact activeClassName="selected"> Register </NavLink>
            </li>
        </ul>  

    return (
        
        <div className="nav_bar">
            {content}
        </div>
    )
}

export const Header = withRouter(HeaderComponent);
//Once we wrap component with withRouter we will have props ( histrory , match and location ) in props