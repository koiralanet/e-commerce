import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { formatDate } from '../../../utils/dataUtil';
import { handleError } from '../../../utils/errorHandler'
import { httpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/toastr';
import './ViewProducts.components.css'

export class ViewProducts extends Component {
    constructor() {
        super()
    
        this.state = {
             isLoading: false,
             products: [],
             dropdown: false
        }
    }
    
    componentDidMount(){
        this.setState({
            isLoading: true
        })
        httpClient.GET('/product', true)
            .then(response =>{
                this.setState({
                    products: response.data
                })
            })
            .catch(err=>{
                handleError(err);
            })
            .finally(()=>{
                this.setState({
                    isLoading: false
                })
            })
    }

    editProduct(id){
        this.props.history.push(`/edit_product/${id}`)
    }

    removeProduct(id, index){
        //ask for confirmation
        // Awesome react component >> modal, overlay
        const confirmation = window.confirm('Are you sure to remove?');
        if(confirmation){
            // Proceced with remove 
            httpClient.DELETE(`/product/${id}`, true)
                .then(response=>{
                    notify.showInfo("product removed");
                    const {products} = this.state;
                    products.splice(index, 1);

                    this.setState({
                        products
                    })
                })
                .catch(err=>{
                    handleError(err)
                })
        }
    }

    
    // lastProduct = (index) =>{
    //    let lengths = index+1;
    //    let arrayProduct = new Array(lengths);
    //    let lastItem = arrayProduct[9];
    //    return lastItem;
    //    console.log('num of product>>', lastItem);
       
    // }
    

    render() {
        let content = this.state.isLoading
            ? <p>show beautiful loader</p>
            :
            <div className="wrapper">
            {
                (this.state.products || []).map((product,index) => (


                    <div className="contain-data" key={index}>

                        <div className="dropdown-container"> 
                            <span className="more-option" htmlFor="editDelete"> <i className="fas fa-ellipsis-h"></i> </span>
                        
                            <div className="dropdown">
                                <p onClick={() => this.editProduct(product._id)}> edit </p>     
                                <p className="p-tag" onClick={() => this.removeProduct(product._id, index)}> delete </p>     
                            </div>
                       
                        </div>
                    <img src="./images/productImg/product.jpg" className="product-img" alt="product.jpg"></img>
                    <h5 className="product-details name">{product.name}</h5>
                    {/* <h6 className="product-details category">category: {product.category}</h6> */}
                    {/* <h6 className="product-details color">{product.color}</h6> */}
                    {/* <h6 className="product-details price">Rs {product.price}</h6> */}
                    <h6 className="product-details price">{formatDate(product.createdAt)}</h6>

                </div>


                ))
            }
            </div>
        

        return (
            
            <div className="container">

            <button className="search-btn"> <img src="./images/search.svg" className="search-svg" alt="searcBtn.jpg"></img> </button>
                
                <div className="product-nav"> 
                   <div>
                   {/* <div className="circle"></div> */}
                   <h2> <strong> Products </strong> </h2>  
                   </div>

                   <div className="sort-filter">
                       <img src="./images/sort.svg" className="sort-svg" alt="sort.jpg"></img>
                       <img src="./images/filter.svg" className="filter-svg" alt="filter.jpg"></img>
                   </div>

                </div>
                <div className="add-option">
                    <img src="./images/sideAddBtn.svg" className="side-svg" alt="wave.jpg"></img>
                    <Link to='/add_product'>
                    <img src="./images/plusBtn.svg" className="plusBtn-svg" onClick={this.onClick} alt="plusBtn"></img>
                    </Link>
                    </div>
                {content}
            </div>
        )
    }
}


