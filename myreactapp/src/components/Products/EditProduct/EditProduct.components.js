import React, { Component } from 'react';
import { httpClient } from '../../../utils/httpClient';
import { ProductForm } from '../ProductForm/ProductForm.components'
import { handleError } from '../../../utils/errorHandler'
import { notify } from '../../../utils/toastr';

export default class EditProduct extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             isLoading: false,
             product: {},
             isSubmitting: false
        }
    }
    
    componentDidMount(){
        this.productId = this.props.match.params['id'];
        this.setState({
            isLoading: true
        })
        httpClient.GET(`/product/${this.productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                handleError(err)
            })
            .finally(() =>{
                this.setState({
                    isLoading: false
                })
            })
    }

    edit = (data) =>{
        this.setState({
            isSubmitting: true
        })
        httpClient.PUT(`/product/${this.productId}`, data ,true)
            .then(response =>{
                notify.showInfo('Product Updated');
                this.props.history.push("/view_products")
            })
            .catch(err=>{
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {

        let content = this.state.isLoading
            ? <p> Loading </p>
            : <ProductForm
              isEditMode = { true}
              submitCallBack = { this.edit}
              isSubmitting = {this.state.isSubmitting}
              productData = {this.state.product}
                ></ProductForm>
        return (
            <>
                {content}
            </>
        )
    }
}
