import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { AppRouting } from './AppRouting';
const App = (args) => {
    console.log('Incommong data>>', args);
    
    // Functional components must return block

    return(
        <div className='document-flow'>
        <AppRouting />
        <ToastContainer />
        </div>
    )


}

export default App;
