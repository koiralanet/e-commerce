import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;

const http = axios.create({
    baseURL: BASE_URL,
    timeout: 10000,
    timeoutErrorMessage: 'Takes too long for response',
});

const getHeaders = (secured) => {
    let options = {
        'Content-Type': 'application/json'
    }
    if(secured){
        options['Authorization'] = localStorage.getItem('token');
    }
    return options;
}

const GET = (url, isSecure= false, params = {}) => {
    return http.get(url, {
        headers: getHeaders(isSecure),
        params
    });

}

const POST = (url, data, isSecure= false, params={}) => {
    return http.post(url, data, {
        headers: getHeaders(isSecure),
        params,
    })

}

const PUT = (url,data, isSecure= false, params={}) => {
    return http.put(url,data,{
        headers: getHeaders(isSecure),
        params
    });

}

const DELETE = (url, isSecure= false, params={}) => {
    return http.delete(url,{
        headers: getHeaders(isSecure),
        params
    });
}

const UPLOAD = (url,data,files = []) => {
    return new Promise((resolve, reject) =>{

        const xhr = new XMLHttpRequest();

    const formData = new FormData();

    // append files to formData
    files.forEach((file) =>{

        formData.append('image', file, file.name)
    })
        // append textual data to formData
        // Data will be object
    for(let key in data){
        formData.append(key,data[key]);
    }
    xhr.onreadystatechange = () =>{
        if(xhr.status === 200){
            resolve(xhr.response)

        }else{
            reject(xhr.response);
        }
    }
    // use xhr mehtods to send formData
    xhr.open('POST',`${BASE_URL}/${url}?token=${localStorage.getItem('token')}`, true);
    xhr.send(formData);

    })
    
}

export const httpClient = {
    GET,
    POST,
    PUT,
    DELETE,
    UPLOAD
}