const redirectToLogin = () =>{
    
}

const redirectToHome = () => {

}

const redirectToDashboard = (props, response) =>{
    //If role admin == > admin Dashboard
    //If role Buyer == > Buyer Dashboard
    if(response.data.user.role === 'seller'){
        props.push('/dashboard')
    }
}

export const Redirect = {
    redirectToDashboard,
    redirectToHome,
    redirectToLogin
};